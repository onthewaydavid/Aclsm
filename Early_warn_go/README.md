# Early_warn_go
服务器异常主动抓取程序 go版

配置文件参数说明：(请不要在真实配置文件中写入任何注释性代码！！)
{
    "default_time": "3",  // 初始化检测时间间隔，单位s
    "interval_time": "2", //发现问题后检测时间间隔，单位s
    "try_num": "2",      //连续几次发现问题后开始抓取系统信息
    "cpu_rates": "5",   //cpu阀值%
    "mem_rates": "5",   //内存阀值%
    "top_load": "5",    //top load
    "Detect_ip": "10.66.48.7", //ping探测地址
    "Push": "True",     //是否推送
    "localhost": "192.168.32.129",  //本机IP
    "push_url": "http://127.0.0.1:8000/loghunter/report/"  //推送url
}